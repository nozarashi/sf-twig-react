import React from 'react';
import {useEffect, useState} from 'react';

export default function Comments() {
  const [comments, setComments] = useState([]);
  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/comments')
      .then(response => response.json())
      .then(json => setComments(json));

  }, []);

  return <div>
    <h1>Comments</h1>
    {comments.map((comment, index) => (
      <div key={index}>
        <h3>{comment.name} ({comment.email})</h3>
        <p>{comment.body}</p>
      </div>
    ))}
  </div>
};
