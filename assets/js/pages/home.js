import React from 'react';
import ReactDOM from 'react-dom/client';
import HelloReact from '../components/HelloReact';
import Comments from '../components/Comments';

ReactDOM.createRoot(document.getElementById('hello-react')).render(<HelloReact />);
ReactDOM.createRoot(document.getElementById('comments')).render(<Comments />);